#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

my %stock = ();

my %vin;
$vin{"bourgogne"} = 30;
$vin{"blanc"} = 20;
$vin{"rosé"} = 40;

my $tva = 20;
my %prix;
$prix{"bourgogne"} = 5;
$prix{"blanc"} = 9;
$prix{"rosé"} = 3;

print calcul(\%vin, \%prix, $tva);
print " €";

sub calcul
{
    my ($vin_ref, $prix_ref, $tva) = @_; # pass 2 referance and tva directly
    my %vin = %{ $vin_ref };
    my %prix = %{ $prix_ref };
    my $prix_total;
    foreach my $key (keys %vin)
    {
        $prix_total += $vin{$key}*$prix{$key}*$tva;
    }
    return $prix_total
}


# Parcourir le stock
foreach my $k (sort { $stock{$b} cmp $stock{$a} } keys %stock)
{
  print $k, '=' , $stock{$k}, "\n";
}

