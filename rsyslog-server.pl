#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use Config::IniFiles;
use IO::Socket::INET;
use Time::Piece;
use Net::Subnet;

#serveur rsyslog récupérant les logs d'un client, ajoutant l'ip source et le timestamp pour le  mettre dans un fichier
# le serveur indique d'abord que l'ip source est autorisé ou log la tentative échoué
# le client envoie les données
# le serveur aquite l'enregistrement des données dans le fichier de log
# code d'erreur :
# 1 : connexion depuis une adresse ip interdite
# 0 : connexion autorisé
# ko : erreur coté serveur lors de la gestion du logs (impossible d'ouvrir le fichier par exemple)
# ok : aucune erreur.


#script fait par Evan ADAM
# lp MI ASSR
# 2018-2019

# parametres par défaut
my $Port      = 15000;
my $LogDir    = "/usr/local/var/log/";
my $LogName   = "message.log";
my $Allow     = subnet_matcher qw(127.0.0.1/32);
my $LogServer = "server.log";
my $debug     = 0;

# récupération des parametres dans le fichier ini
my $cfg = new Config::IniFiles -file => "./config.ini";

if ( $cfg->val( 'root', 'Port' ) ) {
    $Port = $cfg->val( 'root', 'Port' );
}

if ( $cfg->val( 'root', 'LogDir' ) ) {
    $LogDir = $cfg->val( 'root', 'LogDir' );
}

if ( $cfg->val( 'root', 'LogName' ) ) {
    $LogName = $cfg->val( 'root', 'LogName' );
}

if ( $cfg->val( 'root', 'LogServer' ) ) {
    $LogServer = $cfg->val( 'root', 'LogServer' );
}

if ( $cfg->val( 'root', 'Allow' ) ) {
    $Allow = subnet_matcher $cfg->val( 'root', 'Allow[]' );
}

if ( $cfg->val( 'root', 'debug' ) ) { $debug = $cfg->val( 'root', 'debug' ); }

my $fullPathServer = $LogDir . $LogServer;
my $linelogserver = "parametre du socket : port $Port, repertoire des logs $LogDir, nom fichier log standard $LogName, nom fichier log serveer $LogServer, debug $debug";
logserver( $linelogserver, $fullPathServer );

if ($debug) {
    print "debug: paramètres du socket :\n";
    print "debug: Port  " . $Port . "\n";
    print "debug: LogDir  " . $LogDir . "\n";
    print "debug: LogName  " . $LogName . "\n";
    print "debug: LogServer " . $LogServer . "\n";

}

# création de la socket tcp
my $socket = new IO::Socket::INET(
    LocalHost => '0.0.0.0',
    LocalPort => $Port,
    Proto     => 'tcp',
    Listen    => 3,
    Reuse     => 1
);
die "cannot create socket $!\n" unless $socket;

logserver( "le serveur a bien démarré sur le port $Port", $fullPathServer );

if ($debug) {
    print "server waiting for client connection on port " . $Port . "\n";
}

logserver( "démarage du while central", $fullPathServer );
while (1)  # TODO trouver un moyen d'arreter cette boucle de maniere elegante
{
    # attente de la connexion d'un nouveau client
    my $data = "";
    my $client_socket = $socket->accept();

    # récupère les informations du nouveau client
    my $client_address       = $client_socket->peerhost();
    my $client_socket_number = $client_socket->peerport();

    if ($debug) {
        logserver("Debug : nouveau client provenant de $client_address utilisant le port $client_socket_number", $fullPathServer );
    }

    if ( !$Allow->($client_address) ) {
        $data = "1";
        $client_socket->send($data);

        logserver( "tentative frauduleuse de connexion provenant de l'adresse $client_address",$fullPathServer );
        print "client failed :" . $client_address . "\n";
    }
    else {
        $data = "0";
        $client_socket->send($data);

        # lit jusqu'a 1024 caractère de ce qu'envoie le client.
        $client_socket->recv( $data, 1024 );
        if ($debug) {
            print "données recu :  " . $data . "\n";
            print $client_address . "\n";
        }
        my $line = $client_address . ": " . $data;

        print $line . "\n";

        #append $line to log file here.
        my $fullPathLogFile = $LogDir . $LogName;

        if ( logserver( $line, $fullPathLogFile ) == 0 ) {
            print "envoie la réponse ok au client\n";
            $data = "ok";
            $client_socket->send($data);

        }
        else {
            print "erreur lors de l'ouverture du fichier $fullPathLogFile";
            $data = "ko";
            $client_socket->send($data);
            exit 1;

            #fin de programme
        }
    }

    # close client socket
    shutdown( $client_socket, 1 );
}

# server log fin
$socket->close();

# server log arret du serveur

### DEFINITION DES FONCTIONS

sub logserver {
    my ( $text, $file ) = @_;
    my $date = getdate();
    $text = $date . " " . $text;
    open( my $fh, '>>', $file ) or do {
        print "Impossible d'ouvrir le fichier '$file' : $! \n";
        return 1;
    };
    say $fh $text;

    return 0;
}

sub getdate {
    return localtime->strftime('%Y-%m-%d %H-%M-%S');
}
