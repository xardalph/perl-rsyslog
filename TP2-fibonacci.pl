#!/usr/bin/perl -l
use strict;
use warnings FATAL => 'all';

my @fib = (1,1);
my %fibo;
$fib[1]= 1;
$fib[1]= 1;

foreach my $i (1..99)
{
	my $val = $fib[$i - 1] + $fib[$i - 2];
	$fib[$i] = $val;
    $fibo{ $val } = $i;
}
print join("\n",sort({ $a <=> $b } @fib));
my $input = <>;
chomp($input);

if (exists($fibo{$input}))
{
    print "yes : ", $fibo{$input};
}
else
{
    print "noooooooo";
}

