#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use IO::Socket::INET;

# connexion au serveur
my $socket = new IO::Socket::INET(
    PeerHost => '127.0.0.1',
    PeerPort => '15000',
    Proto    => 'tcp',
);
die "cannot connect to the server $!\n" unless $socket;
print "connecté au serveur \n";

# le serveur nous indique si on est autorisé a transmettre des données ou non (0 pour oui 1 pour non)
my $server_allow = "";
$socket->recv( $server_allow, 1024 );

if ( $server_allow == "0" ) {
    print "nous somme sur liste blanche\n";

    # données a envoyer au serveur
    my $req  = 'hello world';
    my $size = $socket->send($req);
    print "sent data of length $size\n";

    # attente de la réponse du serveur.
    my $response = "";
    $socket->recv( $response, 1024 );
    print "received response: $response\n";
    if ( $response eq "ok" ) {
        print "serveur indique : $response \n";
    }
    else {
        print "erreur inconnu du serveur : $response \n";
    }

}
elsif ( $server_allow == "1" ) {
    print "nous ne sommes pas sur la liste blanche, arret.";
    exit 1;
}
else {
    print "reponse inconnu du serveur, arret";
    exit 1;
}

# arret de la connexion avec le serveur
shutdown( $socket, 1 );
$socket->close();
