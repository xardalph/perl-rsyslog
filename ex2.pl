#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

my @words = ();
print "Saisir un mot: ";
while (my $input = <STDIN>)
{
    chomp($input);
	push @words, $input;
    print "saisir un mot: ";
}
print "\n";


print join(',', sort(@words), "\n");
print "\n";

print join(',', reverse(sort(@words)), "\n");

