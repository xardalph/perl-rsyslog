#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use Net::Ping;

my $p;
my $ip="1.1.1.1";
while ( my $ip = <STDIN> ) {
    if ($ip =~ /[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/){
        print $ip;

        $p = Net::Ping->new();
        print "$ip is alive.\n" if $p->ping($ip);
        $p->close();


    }
}