#!/usr/bin/env perl
use strict;
use warnings;

my $text = "";
my $regex = "/^$/";
my $match;
if ($text =~ $regex)
{
	print "yesss";
}
else
{
	print "nooooo";
}

# Initialisation de @fib
my @fib = (1, 1);
my %isfib = ();

__END__
# Calcul des 100 premiers nombres
for (my $i = 2; $i < 100; $i++)
{
	my $val = $fib[$i - 1] + $fib[$i - 2];
	$fib[$i] = $val;
	$isfib{ $val } = $i;
}

print "$_\n" foreach @fib;

print "\nEntrez un nombre : ";
my $input = <>;
chomp($input);
print "\n";

if (exists $isfib{$input})
{
	print "oui, l'indice est $isfib{$input}\n";
}
else
{
	print "non, le nombre n'est pas dans la suite.\n";
}



sleep(3);

for (my $i = 1; $i < 100; $i++)
{
	my $ret = fibonacci($i);
	$isfib{$ret} = $i;
	print "$ret\n";
}

sub fibonacci
{
	$_[0] < 2 ? $_[0] : fibonacci($_[0] - 1) + fibonacci($_[0] - 2);
}

