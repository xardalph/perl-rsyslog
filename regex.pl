#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

#exo6 :
# plaque d'immatriculation ancienne:
# ^[1-9][0-9]{0,4}[a-zA-Z]{1,3}(2a|2b|[0-9]{2})$

# exo 7:
# une opération arithmetique :
# ^[1-9][0-9]{0,4}[a-zA-Z]{1,3}(2a|2b|[0-9]{2})$

# exo 8:
# entier relatif ou réels :
# ^[-]?[0-9]{1,}[\.]?[0-9]{1,}$

my $input = "
frzgre\n
qgtehtshtr
htrshtrd
htrhdr\n
";

my $line = () = $input =~ /\n/g;
my $word = () = $input =~ /[\s][a-zA-Z]/g;
my $cara = () = $input =~ /.{1}/g;
print $line, "  word :  ", $word, " caractere : ", $cara;


